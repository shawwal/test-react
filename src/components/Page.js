import styled from 'styled-components'

export default styled.div`
  h1 {
    font-size: 5vmin;
    font-weight: normal;
    color: #280393;
  }
  p {
    font-size: 0.9375rem;
  }
`
