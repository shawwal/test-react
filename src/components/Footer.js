import styled from 'styled-components'

export default styled.nav`
  padding: 1rem;
  background: #fff;
  border-top: 1px solid #e7e7e7;
  p {
    text-transform: lowercase;
    letter-spacing: 0.1em;
    font-size: 0.875rem;
    text-align: center;
  }
  > * {
    margin-top: 0;
  }
`