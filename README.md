![An Almost Static Stack](https://i.imgur.com/NStGYap.jpg)

# Simple React Apps

This repo is to test React Apps to get 100 scores at Google page insight. [Click Here to check the score](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Freact-100.netlify.com%2F).

## Getting Started

``` shell
git clone https://shawwal@bitbucket.org/shawwal/test-react.git
cd test-react
yarn install
yarn start
```

If you're not into [Yarn](https://yarnpkg.com/), `npm install`, `npm start` and `npm run deploy` all work as well.

## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You can find more information [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Performance and Optimisations

[@stereobooster](https://github.com/stereobooster) has put together a great run thorugh of possible performance optimisations and links to further reading here: https://github.com/stereobooster/react-snap/blob/master/doc/an-almost-static-stack-optimization.md
